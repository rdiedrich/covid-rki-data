defmodule Covid19.Repo.Migrations.CreateInfections do
  use Ecto.Migration

  # IdLandkreis,Altersgruppe,Geschlecht,Meldedatum,Refdatum,IstErkrankungsbeginn,NeuerFall,NeuerTodesfall,NeuGenesen,AnzahlFall,AnzahlTodesfall,AnzahlGenesen
  # 1001,A15-A34,M,2020-10-28,2020-01-19,1,0,-9,0,1,0,1

  def change do
    create table(:infections) do
      add(:lkid, :integer)
      add(:age_group, :string)
      add(:gender, :string)
      add(:reg_date, :date)
      add(:rel_date, :date)
      add(:new_case, :integer)
      add(:new_death, :integer)
      add(:new_recov, :integer)
      add(:num_case, :integer)
      add(:num_death, :integer)
      add(:num_recov, :integer)
    end
  end
end
