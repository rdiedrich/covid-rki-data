defmodule Covid19.Repo.Migrations.AddInfectionsField do
  use Ecto.Migration

  def change do
    alter table(:infections) do
      add(:begin_sick, :integer)
    end
  end
end
