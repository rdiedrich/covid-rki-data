defmodule Covid19 do
  alias Covid19.{Repo, Infection}
  import Ecto.Query

  @moduledoc """
  Documentation for `Covid19`.
  """

  NimbleCSV.define(RKIParser, [])

  #   ["5117", "A80+", "W", "2020-12-25", "2020-12-21", "1", "0", "-9", "0", "1",  "0", "1"]

  defp transform_infection_line([
         lkid,
         age_group,
         gender,
         reg_date,
         rel_date,
         begin_sick,
         new_case,
         new_death,
         new_recov,
         num_case,
         num_death,
         num_recov
       ]) do
    %{
      lkid: String.to_integer(lkid),
      age_group: :binary.copy(age_group),
      gender: :binary.copy(gender),
      reg_date: Date.from_iso8601!(reg_date),
      rel_date: Date.from_iso8601!(rel_date),
      begin_sick: String.to_integer(begin_sick),
      new_case: String.to_integer(new_case),
      new_death: String.to_integer(new_death),
      new_recov: String.to_integer(new_recov),
      num_case: String.to_integer(num_case),
      num_death: String.to_integer(num_death),
      num_recov: String.to_integer(num_recov)
    }
  end

  def import_data do
    "priv/data/infections.csv"
    |> File.stream!([:trim_bom])
    |> Stream.drop(1)
    |> RKIParser.parse_stream()
    |> Stream.map(&transform_infection_line/1)
    |> Stream.chunk_every(1000)
    |> Task.async_stream(fn chunk ->
      Covid19.Repo.insert_all(Covid19.Infection, chunk)
    end)
    |> Stream.run()
  end

  def download_data do
    cmd =
      "curl -s -o priv/data/infections.csv https://media.githubusercontent.com/media/robert-koch-institut/SARS-CoV-2_Infektionen_in_Deutschland/master/Aktuell_Deutschland_SarsCov2_Infektionen.csv"

    {_res, 0} = System.shell(cmd)
  end

  def case_numbers(%Date{} = date) do
    from(i in Infection,
      where: i.reg_date == ^date,
      select: sum(i.num_case)
    )
    |> Repo.one()
  end

  def case_number_range(%Date{} = date) do
    from(i in Infection,
      where: i.reg_date > ^date,
      group_by: i.reg_date,
      select: {i.reg_date, sum(i.num_case)}
    )
    |> Repo.all()
  end
end
