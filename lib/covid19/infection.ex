defmodule Covid19.Infection do
  use Ecto.Schema

  schema "infections" do
    field(:lkid, :integer)
    field(:age_group, :string)
    field(:gender, :string)
    field(:reg_date, :date)
    field(:rel_date, :date)
    field(:begin_sick, :integer)
    field(:new_case, :integer)
    field(:new_death, :integer)
    field(:new_recov, :integer)
    field(:num_case, :integer)
    field(:num_death, :integer)
    field(:num_recov, :integer)
  end
end
