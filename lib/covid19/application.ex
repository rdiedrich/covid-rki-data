defmodule Covid19.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Covid19.Worker.start_link(arg)
      # {Covid19.Worker, arg}
      {Covid19.Repo, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Covid19.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
